﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace CamPOC.classes
{
    public class Captura
    {
        private Camera cam;
        private Thread trd;
        public Captura(Camera cam, Thread trd)
        {
            this.cam = cam;
            this.trd = trd;
        }

        public Camera getCamera()
        {
            return this.cam;
        }
    }

    public static class OrquestradorCameras
    {
        public static List<Captura> capturas = new List<Captura>();

        //sempre que adiciona uma nova câmera, cria uma thread nova para manter imagem atualizada
        public static void Adicionar(Camera objCamera)
        {
            Thread threadCamera = new Thread(() => OrquestradorCameras.ConectaCameras(objCamera));
            threadCamera.Start();

            Captura objCaptura = new Captura(objCamera, threadCamera);
            capturas.Add(objCaptura);
        }

        private static void ConectaCameras(Camera objCamera)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            Task makeSnapshotsTask = Camera.ConnectAsync(objCamera, cancellationTokenSource.Token);
            Console.ReadKey();
            cancellationTokenSource.Cancel();
            makeSnapshotsTask.Wait();
        }

        public static Image getImageFromCamera(String id)
        {
            Image retorno = null;

            foreach(Captura cpt in capturas)
            {
                if (cpt.getCamera()!=null)
                {
                    if (cpt.getCamera().getID()==id)
                    {
                        retorno = cpt.getCamera().getImage();
                        break;
                    }
                }
            }
            return retorno;
        }

    }
}