﻿using RtspClientSharp;
using RtspClientSharp.RawFrames.Video;
using RtspClientSharp.Rtsp;
using RTSPSnapshotMaker.RawFramesDecoding;
using RTSPSnapshotMaker.RawFramesDecoding.DecodedFrames;
using RTSPSnapshotMaker.RawFramesDecoding.FFmpeg;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace CamPOC.classes
{
    public class Camera
    {
        private class Imagem
        {
            public Image imagem;
            public DateTime ultimaLeitura;
        }

        private static readonly Dictionary<FFmpegVideoCodecId, FFmpegVideoDecoder> _videoDecodersMap =
                            new Dictionary<FFmpegVideoCodecId, FFmpegVideoDecoder>();
        private static byte[] _decodedFrameBuffer = new byte[0];

        static int intervalMs = 5000;
        static int lastTimeSnapshotSaved;
        //static string SnapshotPath;

        private bool conectado;
        private string urlrtsp;
        private int interval;
        private int qtdLeituras;
        private string id;

        /// <summary>
        /// Dados da câmera que ira ser conectada.
        /// </summary>
        /// <param name="urlrtsp">Endereço da câmera.</param>
        /// <param name="user">Login para acessar a câmera (se possuir)</param>
        /// <param name="senha">Senha</param>
        /// <param name="interval">tempo em segundos que ira tirar um ScreenShot</param>
        /// <param name="id">Key para acessar a img salva no CACHE do servidor</param>
        public Camera(string urlrtsp, int interval, string id)
        {
            this.urlrtsp = urlrtsp;
            this.interval = interval;
            this.id = id;
            this.qtdLeituras = 0;
        }
        
        public override string ToString()
        {
            return String.Format("URL: {0}, Última leirura: {1}, Quantidade: {2}", this.urlrtsp,
                                                                                    this.getUltimaLeitura(),
                                                                                    this.qtdLeituras);
        }

        public int getIntervalMS()
        {
            return this.interval;
        }
        public Uri GetUri()
        {
            return new Uri (this.urlrtsp);
        }
        public string getID()
        {
            return this.id;
        }

        public DateTime getUltimaLeitura()
        {
            if (HttpRuntime.Cache[this.id] == null)
                return DateTime.MinValue;
            else
                return ((Imagem)HttpRuntime.Cache[this.id]).ultimaLeitura;
        }    

        public Image getImage()
        {
            Imagem imagemDaCamera = null;
            if (HttpRuntime.Cache[this.id]==null) // se não estiver no cache, baixa e cacheia
            {
                imagemDaCamera = new Imagem();
                Image tmp = 
                imagemDaCamera.imagem = ((Imagem)HttpRuntime.Cache[this.id]).imagem;
                imagemDaCamera.ultimaLeitura = DateTime.Now;
            }
            else
            {
                imagemDaCamera = (Imagem) HttpRuntime.Cache[this.id];
                if (imagemDaCamera.ultimaLeitura.AddMilliseconds(this.interval)<DateTime.Now)  // se estiver no cache, mas expirou, baixa e cacheia
                {
                    imagemDaCamera.imagem = ((Imagem) HttpRuntime.Cache[this.id]).imagem;
                    imagemDaCamera.ultimaLeitura = DateTime.Now;
                }
                else
                {
                    // aqui significa que o cache ainda está ativo e não precisa fazer nada
                    imagemDaCamera = (Imagem) HttpRuntime.Cache[this.id];
                }
            }

            this.qtdLeituras++;
            HttpRuntime.Cache[this.id] = imagemDaCamera;

            return (Image)imagemDaCamera.imagem;
        }

        public static async Task ConnectAsync(Camera camera, CancellationToken token)
        {
            try
            {
                ConnectionParameters connectionParameters = new ConnectionParameters(camera.GetUri());

                intervalMs = camera.getIntervalMS() * 1000;
                lastTimeSnapshotSaved = Environment.TickCount - intervalMs;

                using (var rtspClient = new RtspClient(connectionParameters))
                {
                    //rtspClient.FrameReceived += OnFrameReceived;
                    rtspClient.FrameReceived += (sender, rawFrame) =>
                    {
                        if (!(rawFrame is RawVideoFrame rawVideoFrame))
                            return;

                        FFmpegVideoDecoder decoder = GetDecoderForFrame(rawVideoFrame);
                        if (!decoder.TryDecode(rawVideoFrame, out DecodedVideoFrameParameters decodedFrameParameters))
                            return;

                        int ticksNow = Environment.TickCount;

                        if (Math.Abs(ticksNow - lastTimeSnapshotSaved) < intervalMs) return;

                        lastTimeSnapshotSaved = ticksNow;

                        int bufferSize;

                        bufferSize = decodedFrameParameters.Height *
                                     ImageUtils.GetStride(decodedFrameParameters.Width, RTSPSnapshotMaker.RawFramesDecoding.PixelFormat.Abgr32);

                        if (_decodedFrameBuffer.Length != bufferSize)
                            _decodedFrameBuffer = new byte[bufferSize];
                        
                        var bufferSegment = new ArraySegment<byte>(_decodedFrameBuffer);

                        var postVideoDecodingParameters = new PostVideoDecodingParameters(RectangleF.Empty,
                            new Size(decodedFrameParameters.Width, decodedFrameParameters.Height),
                            ScalingPolicy.Stretch, RTSPSnapshotMaker.RawFramesDecoding.PixelFormat.Bgr24, ScalingQuality.Bicubic);

                        IDecodedVideoFrame decodedFrame = decoder.GetDecodedFrame(bufferSegment, postVideoDecodingParameters);

                        ArraySegment<byte> frameSegment = decodedFrame.DecodedBytes;

                        string snapshotName = decodedFrame.Timestamp.ToString("O").Replace(":", "_") + ".jpg";
                        
                        ToImage(decodedFrameParameters.Width, decodedFrameParameters.Height, snapshotName, System.Drawing.Imaging.PixelFormat.Format24bppRgb, frameSegment.Array, camera); 
                    };

                    while (true)
                    {
                        System.Diagnostics.Debug.WriteLine("Connecting..." + camera.id);
                        try
                        {
                            await rtspClient.ConnectAsync(token);
                        }
                        catch (OperationCanceledException)
                        {
                            return;
                        }
                        catch (RtspClientException e)
                        {
                            System.Diagnostics.Debug.WriteLine(e.ToString());
                            continue;
                        }

                        System.Diagnostics.Debug.WriteLine("Connected." + camera.id);

                        try
                        {
                            await rtspClient.ReceiveAsync(token);
                        }
                        catch (OperationCanceledException)
                        {
                            return;
                        }
                        catch (RtspClientException e)
                        {
                            System.Diagnostics.Debug.WriteLine(e.ToString());
                        }
                    }
                }
            }
            catch (OperationCanceledException)
            {
            }
        }
           
        private static void ToImage(int Width, int Height, string imgName, System.Drawing.Imaging.PixelFormat pixelFormat, byte[] rgbValues, Camera camera)
        {
            Bitmap bitMap = new Bitmap(Width, Height, pixelFormat);

            Rectangle BoundsRect = new Rectangle(0, 0, Width, Height);

            BitmapData bitmapData = bitMap.LockBits(BoundsRect, ImageLockMode.WriteOnly, bitMap.PixelFormat);

            IntPtr _dataPointer = bitmapData.Scan0;

            int _byteData = bitmapData.Stride * bitMap.Height;

            Marshal.Copy(rgbValues, 0, _dataPointer, _byteData);

            bitMap.UnlockBits(bitmapData);

            ImageCodecInfo jgpEncoder = GetEncoder(ImageFormat.Jpeg);

            System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.Quality;

            EncoderParameters encoderParams = new EncoderParameters(1);
            EncoderParameter myEncoderParameter = new EncoderParameter(encoder, 50L);

            encoderParams.Param[0] = myEncoderParameter;

            
            Imagem tmp = new Imagem();
            tmp.imagem = bitMap;
            tmp.ultimaLeitura = DateTime.Now;
            
            using (var ms = new MemoryStream())
                HttpRuntime.Cache[camera.id] = tmp;

            //bitMap.Save(SnapshotPath + imgName, jgpEncoder, encoderParams);
            //Console.WriteLine($"New frame {imgName} saved");
            System.Diagnostics.Debug.WriteLine($"New frame {imgName} saved from Key {camera.id}");
        }

        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {

            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        private static FFmpegVideoDecoder GetDecoderForFrame(RawVideoFrame videoFrame)
        {
            FFmpegVideoCodecId codecId = DetectCodecId(videoFrame);
            if (!_videoDecodersMap.TryGetValue(codecId, out FFmpegVideoDecoder decoder))
            {
                decoder = FFmpegVideoDecoder.CreateDecoder(codecId);
                _videoDecodersMap.Add(codecId, decoder);
            }
            return decoder;
        }

        private static FFmpegVideoCodecId DetectCodecId(RawVideoFrame videoFrame)
        {
            if (videoFrame is RawJpegFrame)
                return FFmpegVideoCodecId.MJPEG;
            if (videoFrame is RawH264Frame)
                return FFmpegVideoCodecId.H264;

            throw new ArgumentOutOfRangeException(nameof(videoFrame));
        }
    }
}