﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using RtspClientSharp;
using RtspClientSharp.RawFrames;
using RtspClientSharp.RawFrames.Video;
using RtspClientSharp.Rtsp;
using RTSPSnapshotMaker.RawFramesDecoding;
using RTSPSnapshotMaker.RawFramesDecoding.DecodedFrames;
using RTSPSnapshotMaker.RawFramesDecoding.FFmpeg;
using CamPOC.classes;
using System.Web;
using System.Configuration;

namespace CamPOC
{
    public partial class painel : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            Atualizar();
        }
      
        protected void btnAtualizar_Click(object sender, EventArgs e)
        {
            Atualizar();
            //Thread thread = new Thread(conectarCamera);
            //thread.Start();
        }

        private static Options GetConfiguration()
        {
            Options options = new Options();

            options.Uri= new Uri(ConfigurationManager.AppSettings["URI"]);
            options.Interval = Int32.Parse(ConfigurationManager.AppSettings["interval"]);
            //options.key = ConfigurationManager.AppSettings["key"];

            return options;
        }

        private void Atualizar()
        {
            lblSituacao.Text = "";
            foreach (var cam in OrquestradorCameras.capturas)
            {
                lblSituacao.Text += "<br/><a href='imagem.aspx?idcamera="+ cam.getCamera().getID()+ "' target='_blank'>" + cam.getCamera()+"</a>";
            }
        }

        public class Options
        {
            public Uri Uri { get; set; }
            public int Interval { get; set; }
            public string key { get; set; }
        }

        
    }
}