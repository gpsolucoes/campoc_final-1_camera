﻿using CamPOC.classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using static CamPOC.painel;

namespace CamPOC
{
    public class Global : System.Web.HttpApplication
    {
        public static List<Camera> cameras = new List<Camera>();

        protected void Application_Start(object sender, EventArgs e)
        {
            //OrquestradorCameras.Adicionar(new Camera("rtsp://cameras.cijun.sp.gov.br:7447/078d2a5f-a1b7-334f-9020-cdde76ad7012_0", 5, "cam1"));
            //OrquestradorCameras.Adicionar(new Camera("rtsp://cameras.cijun.sp.gov.br:7447/78a812e0-36b9-354f-a354-564e6d001728_1", 4, "cam2"));
            //OrquestradorCameras.Adicionar(new Camera("rtsp://184.72.239.149/vod/mp4:BigBuckBunny_115k.mov", 5, "flavio"));
            OrquestradorCameras.Adicionar(new Camera("rtsp://testcam:T35tC1jun@aovivo.cijun.sp.gov.br:554/cam/realmonitor?channel=1&subtype=0", 4, "CAM_2"));
            
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}