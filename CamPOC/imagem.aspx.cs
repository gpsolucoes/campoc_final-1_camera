﻿using CamPOC.classes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Image = System.Drawing.Image;

namespace CamPOC
{
    public partial class imagem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Response.ClearContent();
            try
            {
                String strID = Request.QueryString["idcamera"];

                Response.ContentType = "image/png";

                Image image = OrquestradorCameras.getImageFromCamera(strID);
                image.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Png);


            }
            catch (Exception ex)
            {
                Response.ContentType = "text/plain";
                Response.Write(ex.Message);
            }

            Response.End();
        }
    }
}